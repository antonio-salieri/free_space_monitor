#!/bin/bash

if [ ! $1  ]
then
exit 127
fi

cd $1

for ((day=0;day<366;day++));
do
ts_now=`date +%s`
ts_file=`expr $ts_now - $day \* 3600 \* 24`
filename=`date --date=@$ts_file +%Y.%m.%d.`
touch -f "$filename" -d `date --date=@$ts_file +%Y%m%d`
done
