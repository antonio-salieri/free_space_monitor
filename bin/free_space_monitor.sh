#!/bin/bash

#set -x
config=/usr/local/etc/free_space_monitor.conf
myname="${0##*/} (PID $$)"

LOG_FILE=/tmp/free_space_monitor.log
MONITOR_DIR=/tmp
FILE_TTL=30

# Function to set $maxload from $config:
read_config()
{
  if [ ! -r $config ]
  then
  echo "$myname: Unable to read config file '${config}'." >> $LOG_FILE
  exit 100
  fi

  if [ "$1" == -v ]
  then
  echo "$myname: reading $config file" >> $LOG_FILE
  fi
  . $config
}

read_config                 # set $maxloads array
trap 'read_config -v' HUP   # On SIGHUP, re-read $config

echo "$myname: Started at "`date` >> $LOG_FILE

if [ -z $MONITOR_DIR ]
then
echo "$myname: No directory defined for monitoring." >> $LOG_FILE
exit 127
fi

if [ ! -d $MONITOR_DIR ]
then
echo "$myname: Monitoring directory does not exists." >> $LOG_FILE
exit 126
fi

# Endless loop until killed by signal 15 (etc.):
while :
do
  free_space_percent=`df -H $MONITOR_DIR | grep -vE '^Filesystem' | awk '{ print $5 " " $1 }'|cut -d'%' -f1`
  echo $free_space_percent
  if [ -z $free_space_percent ]
  then
  echo $myname: Unable to detect partition free space! >> $LOG_FILE
  exit 125
  fi

  if [ $USAGE_TRIGGER -le $free_space_percent ]
  then
  echo "~~~~~~~~~~~~~~~~~~~~~~~" >> $LOG_FILE
  echo $myname: `date` >> $LOG_FILE
  echo -e "\t Warrning directory '$MONITOR_DIR' fs usage is $free_space_percent% (config usage trigger for file removal is $USAGE_TRIGGER%)." >> $LOG_FILE
  echo -e "\t Removing files from $MONITOR_DIR which are older than $FILE_TTL days." >> $LOG_FILE
  find $MONITOR_DIR -mtime +$FILE_TTL -type f -exec rm -vf '{}' \; >> $LOG_FILE
  fi

  sleep 10
done
