#!/bin/bash

echo ""
echo "Free space monitor Installer"
echo ""
echo "Extracting file into /usr/local"
SKIP=`awk '/^__TARFILE_FOLLOWS__/ { print NR + 1; exit 0; }' $0`

#remember our file name
THIS=`pwd`/$0

# Go into /usr/local directory
cd /usr/local
# take the tarfile and pipe it into tar
tail -n +$SKIP $THIS | tar -xz

cp -av /usr/local/_rc.d/free_space_monitor /etc/rc.d/free_space_monitor
rm -rv /usr/local/_rc.d

chkconfig add free_space_monitor
chkconfig free_space_monitor on
service free_space_monitor start

#
# place any bash script here you need.
# Any script here will happen after the tar file extract.
echo "Finished"
exit 0

# NOTE: Don't place any newline characters after the last line below.
__TARFILE_FOLLOWS__
